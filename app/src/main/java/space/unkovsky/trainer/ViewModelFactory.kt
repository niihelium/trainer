package space.unkovsky.trainer

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import space.unkovsky.trainer.data.local.DataRepository
import space.unkovsky.trainer.vm.MathTaskSetupViewModel
import space.unkovsky.trainer.vm.MathTaskViewModel
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by nii on 12/7/17.
 */
@Singleton
class ViewModelFactory @Inject constructor(private val dataRepository: DataRepository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(MathTaskSetupViewModel::class.java) ->
                        MathTaskSetupViewModel(dataRepository)
                    isAssignableFrom(MathTaskViewModel::class.java) ->
                        MathTaskViewModel(dataRepository)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T
}