package space.unkovsky.trainer.di.modules

import dagger.Module
import dagger.Provides
import space.unkovsky.trainer.data.local.AppDatabase
import space.unkovsky.trainer.data.local.DataRepository
import javax.inject.Singleton

/**
 * Created by bsobat on 26/05/17.
 */
@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideDataRepository(db: AppDatabase): DataRepository {
        return DataRepository(db)
    }
}