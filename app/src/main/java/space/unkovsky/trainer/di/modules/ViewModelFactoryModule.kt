package space.unkovsky.trainer.di.modules

import dagger.Module
import dagger.Provides
import space.unkovsky.trainer.ViewModelFactory
import space.unkovsky.trainer.data.local.DataRepository
import javax.inject.Singleton

/**
 * Created by nii on 12/7/17.
 */
@Module
class ViewModelFactoryModule {
    @Provides
    @Singleton
    fun provideViewModelFactory(dataRepository: DataRepository): ViewModelFactory {
        return ViewModelFactory(dataRepository)
    }
}