package space.unkovsky.trainer.di.modules

import android.app.Application
import android.arch.persistence.room.Room
import dagger.Module
import dagger.Provides
import space.unkovsky.trainer.data.local.AppDatabase
import javax.inject.Singleton

/**
 * Created by bsobat on 26/05/17.
 */
@Module
class DatabaseModule(val dbName: String) {
    @Provides
    @Singleton
    fun provideCatalogRepository(app: Application): AppDatabase {
        return Room.databaseBuilder(app, AppDatabase::class.java, dbName).build()
    }
}