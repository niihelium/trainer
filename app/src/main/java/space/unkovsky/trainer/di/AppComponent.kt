package space.unkovsky.trainer.di

import android.app.Application
import dagger.Component
import space.unkovsky.trainer.di.modules.AppModule
import space.unkovsky.trainer.di.modules.DatabaseModule
import space.unkovsky.trainer.di.modules.RepositoryModule
import space.unkovsky.trainer.di.modules.ViewModelFactoryModule
import space.unkovsky.trainer.view.MathTaskFragment
import space.unkovsky.trainer.view.MathTaskSetupFragment
import javax.inject.Singleton

/**
 * Created by nii on 12/4/17.
 */
@Singleton
@Component(
        modules = arrayOf(
                AppModule::class,
                DatabaseModule::class,
                RepositoryModule::class,
                ViewModelFactoryModule::class)
)
interface AppComponent {
    fun inject(mathTaskSetupFragment: MathTaskSetupFragment)
    fun inject(mathTaskFragment: MathTaskFragment)

    companion object Factory {
        fun create(app: Application, dbName: String): AppComponent {
            return DaggerAppComponent.builder()
                    .appModule(AppModule(app))
                    .databaseModule(DatabaseModule(dbName))
                    .repositoryModule(RepositoryModule())
                    .viewModelFactoryModule(ViewModelFactoryModule())
                    .build()
        }
    }
}