package space.unkovsky.trainer.vm

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.view.View
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import space.unkovsky.trainer.data.Session
import space.unkovsky.trainer.data.Task
import space.unkovsky.trainer.data.TaskGenerator
import space.unkovsky.trainer.data.TaskParams
import space.unkovsky.trainer.data.local.DataRepository
import java.util.concurrent.TimeUnit

/**
 * Created by nii on 12/4/17.
 */
class MathTaskViewModel(
        private val dataRepository: DataRepository
) : ViewModel() {
    val currentTaskObservable = MutableLiveData<Task>()
    val countdownProgress = MutableLiveData<Int>()
    private lateinit var currentTask: Task
    val currentSession = Session(System.currentTimeMillis(), theme = 1)

    lateinit var taskParams: TaskParams

    var started = false

    fun start(params: TaskParams) {
        taskParams = params
        updateTask()
        started = true
    }


    fun onDetach(view: View) {
        saveSession()
    }

    fun checkAnswer(userAnswer: String): Boolean {
        val result = (currentTask.answers == userAnswer)
        if (result) {
            currentSession.questionsCorrect += 1
        }
        updateTask()
        saveSession()
        return result
    }

    fun runObservable(finishTime: Int): Observable<Long> {
        val precision = 100L
        val take = (finishTime * 1000) / precision + 1

        return Observable.interval(precision, TimeUnit.MILLISECONDS)
                .take(take)
                .map { t ->
                    1000 - ((t * precision) * 1000 / (finishTime * 1000))
                }

    }


    private fun saveSession() {
        currentSession.duration = sessionElapsed()
        dataRepository.saveSession(currentSession)

    }

    private fun sessionElapsed() = System.currentTimeMillis() - currentSession.startTime

    private fun updateTask() {

        TaskGenerator.generateMathTask(taskParams).let {
            currentTaskObservable.value = it
            currentTask = it
        }
        currentSession.questionsTotal += 1
        runObservable(20)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { time -> time.toInt() }
                .subscribe(
                        { time ->
                            countdownProgress.value = time
                        })
    }

}