package space.unkovsky.trainer.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import space.unkovsky.trainer.data.TaskParams
import space.unkovsky.trainer.data.local.DataRepository
import space.unkovsky.trainer.data.local.DbConstants

/**
 * Created by nii on 12/4/17.
 */
class MathTaskSetupViewModel(private val dataRepository: DataRepository) : ViewModel() {
    val taskParams: LiveData<TaskParams> = dataRepository.getTaskParams(DbConstants.THEME_MENTAL_MATH)

}