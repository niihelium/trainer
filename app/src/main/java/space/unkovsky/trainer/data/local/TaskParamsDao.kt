package space.unkovsky.trainer.data.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import io.reactivex.Flowable
import space.unkovsky.trainer.data.Session
import space.unkovsky.trainer.data.TaskParams

/**
 * Created by nii on 10.09.17.
 */
@Dao
interface TaskParamsDao {
    @Query("SELECT * FROM tasks_params")
    fun getAll(): LiveData<List<TaskParams>>

    @Query("SELECT * FROM tasks_params WHERE taskId = :theme")
    fun getByTheme(theme: Int): LiveData<TaskParams>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(session: TaskParams)

    @Insert
    fun insertAll(sessions: List<TaskParams>)

    @Delete
    fun delete(session: TaskParams)
}