package space.unkovsky.trainer.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import space.unkovsky.trainer.data.Note
import space.unkovsky.trainer.data.Session
import space.unkovsky.trainer.data.Task
import space.unkovsky.trainer.data.TaskParams

/**
 * Created by nii on 10.09.17.
 */
@Database(entities = arrayOf(
        Session::class,
        Task::class,
        Note::class,
        TaskParams::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun sessionDao(): SessionDao
    abstract fun taskDao(): TaskDao
    abstract fun notesDao(): NotesDao
    abstract fun taskParamsDao(): TaskParamsDao

}