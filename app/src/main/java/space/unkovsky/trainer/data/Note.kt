package space.unkovsky.trainer.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import space.unkovsky.trainer.data.local.DbConstants.Companion.NOTES_TABLE

/**
 * Created by nii on 10.09.17.
 */
@Entity(tableName = NOTES_TABLE)
class Note(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        var content: String,
        val subject: Int,
        val theme: Int)