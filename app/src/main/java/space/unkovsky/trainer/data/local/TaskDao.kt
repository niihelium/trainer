package space.unkovsky.trainer.data.local

import android.arch.persistence.room.*
import io.reactivex.Flowable
import space.unkovsky.trainer.data.Session
import space.unkovsky.trainer.data.Task

/**
 * Created by nii on 10.09.17.
 */
@Dao
interface TaskDao {
    @Query("SELECT * FROM tasks")
    fun getAll(): Flowable<List<Task>>

    @Query("SELECT * FROM tasks WHERE themeId = :theme")
    fun getAllByTheme(theme: Int): Flowable<List<Task>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(session: Session)

    @Insert
    fun insertAll(sessions: List<Session>)

    @Delete
    fun delete(session: Session)
}