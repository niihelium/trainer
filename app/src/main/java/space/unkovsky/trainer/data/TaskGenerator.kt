package space.unkovsky.trainer.data

import android.util.Log
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import net.sourceforge.jeval.Evaluator
import java.util.*

/**
 * Created by nii on 09.09.17.
 */
class TaskGenerator {

//    private val eventSubject = PublishSubject.create<Task>()
//
//    fun getEventObservable(): Observable<Task> {
//        return eventSubject
//    }
//
//    fun generateTask(taskParams: TaskParams) {
//        eventSubject.onNext(
//                generateMathTask(taskParams)
//        )
//    }


    companion object {
        val min = 2
        val max = 9

        fun generateMathTask(taskParams: TaskParams): Task {
            val action = randAction(taskParams)
            val maxValue = Math.pow(10.0, taskParams.maxDigits.toDouble()).toInt() - 1
            val expression =
                    when (action) {
                        "*" -> "${randInt(min, maxValue)} $action ${randInt(min, maxValue)}"
                        "/" -> getDivisionExpression(maxValue)
                        else -> "${randInt(min, maxValue)} $action ${randInt(min, maxValue)}"
                    }
            Log.d("TaskGenerator", expression)
            return Task(
                    task = expression,
                    answers = Evaluator().evaluate(expression).substringBefore("."))
        }

        private fun getDivisionExpression(maxNumber: Int): String {
            val divisor = randInt(min, max)
            val dividend = randInt(divisor, maxNumber)
            val intNumber = dividend / divisor
            return "${intNumber * divisor} / $divisor"
        }

        private fun randInt(min: Int, max: Int) = Random().nextInt(max - min + 1) + min
        private fun randAction(taskParams: TaskParams): String {
            val actions = arrayListOf<String>()
            if (taskParams.sum)
                actions.add("+")
            if (taskParams.minus)
                actions.add("-")
            if (taskParams.multiply)
                actions.add("*")
            if (taskParams.divide)
                actions.add("/")

            return actions[Random().nextInt(actions.size)]
        }


//        fun getTaskByType(type: String) = when(type) {
//            TASK_TYPE_INPUT -> Task()
//
//            TASK_TYPE_ONE_OF_FOUR -> Task()
//            else -> Throwable
//        }

        fun getTaskByTheme(theme: Int) = Task(1, 2, 3, "DFD", "0")


    }
}
