package space.unkovsky.trainer.data

/**
 * Created by nii on 11/8/17.
 */
class Theme(val id: Int, val name: String){
    companion object {
        val ID = "id"
        val NAME = "name"
    }
}