package space.unkovsky.trainer.data.local

import android.arch.persistence.room.*
import io.reactivex.Flowable
import space.unkovsky.trainer.data.Note
import space.unkovsky.trainer.data.local.DbConstants.Companion.NOTES_TABLE

/**
 * Created by nii on 10.09.17.
 */
@Dao
interface NotesDao {
    @Query("SELECT * FROM $NOTES_TABLE")
    fun getAll(): Flowable<List<Note>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(session: Note)

    @Insert
    fun insertAll(sessions: List<Note>)

    @Delete
    fun delete(session: Note)
}