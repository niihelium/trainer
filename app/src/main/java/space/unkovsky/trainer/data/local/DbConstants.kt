package space.unkovsky.trainer.data.local

/**
 * Created by nii on 12/1/17.
 */

final class DbConstants {
    companion object {
        const val NOTES_TABLE = "notes"
        const val THEME_MENTAL_MATH = 0
    }
}
