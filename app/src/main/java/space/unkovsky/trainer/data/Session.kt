package space.unkovsky.trainer.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by nii on 10.09.17.
 */
@Entity(tableName = "sessions")
class Session(
        @PrimaryKey
        val startTime: Long,
        var duration: Long = 0,
        val theme: Int,
        var questionsTotal: Int = 0,
        var questionsCorrect: Int = 0)