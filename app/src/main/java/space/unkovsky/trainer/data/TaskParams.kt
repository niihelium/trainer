package space.unkovsky.trainer.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/**
 * Created by nii on 12/2/17.
 */
@Entity(tableName = "tasks_params")
class TaskParams(
        @PrimaryKey
        val taskId: Int,
        val sum: Boolean = true,
        val minus: Boolean = true,
        val multiply: Boolean = true,
        val divide: Boolean = true,
        val maxActions: Int = 1,
        val maxDigits: Int = 2
) : Serializable