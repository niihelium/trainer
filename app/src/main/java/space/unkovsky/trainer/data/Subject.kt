package space.unkovsky.trainer.data

/**
 * Created by nii on 09.09.17.
 */
class Subject(val id: Int, val name: String){
    companion object {
        val ID = "id"
        val NAME = "name"
    }
}
