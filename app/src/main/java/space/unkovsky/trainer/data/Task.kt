package space.unkovsky.trainer.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by nii on 09.09.17.
 */
/**
 * Simple task with question and one input
 * -Input can be numeric or alphabetic
 *
 * Correct answer should always be first
 */

val INPUT_TYPE_NUMERIC = "numeric"
val INPUT_TYPE_ALPHABETIC = "alphabetic"

val TASK_TYPE_INPUT = "input"
val TASK_TYPE_ONE_OF_FOUR = "one_of_four"

@Entity(tableName = "tasks")
open class Task(
        @PrimaryKey
        val taskId: Int = 0,
        val themeId: Int = 0,
        val subjectId: Int = 0,
        val task: String,
        val answers: String,
        val inputType: String = INPUT_TYPE_NUMERIC,
        val taskType: String = TASK_TYPE_INPUT) {

}