package space.unkovsky.trainer.data.local

import android.arch.lifecycle.LiveData
import space.unkovsky.trainer.data.Session
import space.unkovsky.trainer.data.TaskParams
import javax.inject.Inject
import kotlin.concurrent.thread

/**
 * Created by nii on 12/4/17.
 */
class DataRepository @Inject constructor(val db: AppDatabase) {
    fun getTaskParams(themeId: Int): LiveData<TaskParams> =
            db.taskParamsDao().getByTheme(themeId)

    fun saveSession(session: Session){
        thread(start = true) {
            db.sessionDao().insert(session)
        }

    }
}