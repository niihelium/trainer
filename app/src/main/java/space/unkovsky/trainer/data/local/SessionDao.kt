package space.unkovsky.trainer.data.local

import android.arch.persistence.room.*
import io.reactivex.Flowable
import space.unkovsky.trainer.data.Session

/**
 * Created by nii on 10.09.17.
 */
@Dao
interface SessionDao {
    @Query("SELECT * FROM sessions")
    fun getAll(): Flowable<List<Session>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(session: Session)

    @Insert
    fun insertAll(sessions: List<Session>)

    @Delete
    fun delete(session: Session)
}