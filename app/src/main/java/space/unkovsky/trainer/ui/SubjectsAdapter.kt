package space.unkovsky.trainer.ui

/**
 * Created by nii on 9/2/17.
 */
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.subject_card.view.*
import space.unkovsky.trainer.R
import space.unkovsky.trainer.data.Subject

class SubjectsAdapter(private val items: List<Subject>,
                      private val itemClick: (Subject) -> Unit) :
        BaseAdapter<Subject>(items, itemClick) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.subject_card, parent, false)
        return ViewHolder(view, itemClick)
    }

    class ViewHolder(view: View, private val itemClick: (Subject) -> Unit)
        : BaseAdapter.ViewHolder<Subject>(view, itemClick) {
        override fun bindItem(item: Subject) {
            with(item) {
                itemView.titleTV.text = item.name
                itemView.setOnClickListener { itemClick(this) }
            }
        }
    }
}