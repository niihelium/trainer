package space.unkovsky.trainer.ui

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by nii on 11/8/17.
 */
abstract class BaseAdapter<T>(private val items: List<T>,
                              private val itemClick: (T) -> Unit)
    : RecyclerView.Adapter<BaseAdapter.ViewHolder<T>>() {

    override fun getItemCount() = items.size
    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) {
        holder.bindItem(items[position])
    }

    abstract class ViewHolder<T>(view: View, private val vItemClick: (T) -> Unit)
        : RecyclerView.ViewHolder(view) {

        abstract fun bindItem(item: T)

    }
}