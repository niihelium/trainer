package space.unkovsky.trainer.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.theme_card.view.*
import space.unkovsky.trainer.R
import space.unkovsky.trainer.data.Theme

/**
 * Created by nii on 11/8/17.
 */
class ThemesAdapter(private val items: List<Theme>,
                    private val itemClick: (Theme) -> Unit) :
        BaseAdapter<Theme>(items, itemClick) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.theme_card, parent, false)
        return ViewHolder(view, itemClick)
    }

    class ViewHolder(view: View, private val itemClick: (Theme) -> Unit)
        : BaseAdapter.ViewHolder<Theme>(view, itemClick) {
        override fun bindItem(item: Theme) {
            with(item) {
                itemView.titleTV.text = item.name
                itemView.start_btn.setOnClickListener { itemClick(this) }
            }
        }
    }
}