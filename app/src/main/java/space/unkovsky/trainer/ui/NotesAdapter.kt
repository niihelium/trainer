package space.unkovsky.trainer.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.note_item.view.*
import space.unkovsky.trainer.R
import space.unkovsky.trainer.data.Note

class NotesAdapter(var items: List<Note>,
                   private val itemClick: (Note) -> Unit) :
        BaseAdapter<Note>(items, itemClick) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.note_item, parent, false)
        return ViewHolder(view, itemClick)
    }

    class ViewHolder(view: View, private val itemClick: (Note) -> Unit)
        : BaseAdapter.ViewHolder<Note>(view, itemClick) {
        override fun bindItem(item: Note) {
            with(item) {
                //itemView.start_time.text = item.startTime.toString()
                itemView.subject.text = item.subject.toString()
                itemView.note_content.text = item.content
                itemView.setOnClickListener { itemClick(this) }
            }
        }
    }
}