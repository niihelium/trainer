package space.unkovsky.trainer.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.stats_item.view.*
import space.unkovsky.trainer.R
import space.unkovsky.trainer.data.Session
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class SessionsAdapter(var items: List<Session>,
                      private val itemClick: (Session) -> Unit) :
        BaseAdapter<Session>(items, itemClick) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.stats_item, parent, false)
        return ViewHolder(view, itemClick)
    }

    class ViewHolder(view: View, private val itemClick: (Session) -> Unit)
        : BaseAdapter.ViewHolder<Session>(view, itemClick) {
        override fun bindItem(item: Session) {
            with(item) {
                //itemView.start_time.text = item.startTime.toString()
                itemView.start_time.text = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.getDefault()).format(Date(item.startTime))
                //itemView.total_time.text = item.duration.toString()
                itemView.total_time.text = SimpleDateFormat("HH:mm:ss").format(Date(item.duration))
                itemView.total_questions.text = item.questionsTotal.toString()
                itemView.correct_answers.text = item.questionsCorrect.toString()
                itemView.answers_percentage.text = (100.0 / item.questionsTotal * item.questionsCorrect).toString()
                itemView.setOnClickListener { itemClick(this) }
            }
        }
    }
}