package space.unkovsky.trainer.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_main.view.*
import space.unkovsky.trainer.MainActivity
import space.unkovsky.trainer.R
import space.unkovsky.trainer.kotlin.replaceFragmentInActivity

/**
 * Created by nii on 12/3/17.
 */
class MainFragment : Fragment() {
    companion object {
        fun newInstance() = MainFragment()
        const val TAG = "MainFragment"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        view.start_btn.setOnClickListener {
            (activity as MainActivity).apply {
                MathTaskSetupFragment.newInstance().let {
                    replaceFragmentInActivity(it, R.id.contentFrame)
                }
            }
        }
        return view
    }
}