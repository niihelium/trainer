package space.unkovsky.trainer.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.controller_math_task_setup.view.*
import space.unkovsky.trainer.App
import space.unkovsky.trainer.MainActivity
import space.unkovsky.trainer.R
import space.unkovsky.trainer.ViewModelFactory
import space.unkovsky.trainer.data.TaskParams
import space.unkovsky.trainer.kotlin.replaceFragmentInActivity
import space.unkovsky.trainer.vm.MathTaskSetupViewModel
import javax.inject.Inject

/**
 * Created by nii on 12/3/17.
 */
class MathTaskSetupFragment : Fragment() {
    companion object {
        fun newInstance() = MathTaskSetupFragment()
        const val TAG = "MathTaskSetupFragment"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: MathTaskSetupViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
        viewModel = ViewModelProviders.of(activity, viewModelFactory).get(MathTaskSetupViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.controller_math_task_setup, container, false)

        viewModel.taskParams.observe(this, Observer {
            it?.let { updateUi(it) }
        })
        //ApplyDataBinding
        view.start_btn.setOnClickListener {
            val taskParams = TaskParams(0,
                    view.checkbox_plus.isChecked,
                    view.checkbox_minus.isChecked,
                    view.checkbox_multiply.isChecked,
                    view.checkbox_divide.isChecked,
                    view.actions.text.toString().toInt(),
                    view.numbers.text.toString().toInt())
            (activity as MainActivity).apply {
                (activity as MainActivity).apply {
                    MathTaskFragment.newInstance(taskParams).let {
                        replaceFragmentInActivity(it, R.id.contentFrame)
                    }
                }
            }
        }
        return view
    }

    private fun updateUi(taskParams: TaskParams) {
        view?.apply {
            checkbox_plus.isChecked = taskParams.sum
            checkbox_minus.isChecked = taskParams.minus
            checkbox_multiply.isChecked = taskParams.multiply
            checkbox_divide.isChecked = taskParams.divide
            numbers.setText(taskParams.maxDigits.toString())
        }
    }
}