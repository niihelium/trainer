package space.unkovsky.trainer.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.controller_task_input.view.*
import space.unkovsky.trainer.App
import space.unkovsky.trainer.R
import space.unkovsky.trainer.ViewModelFactory
import space.unkovsky.trainer.data.Task
import space.unkovsky.trainer.data.TaskParams
import space.unkovsky.trainer.vm.MathTaskViewModel
import java.io.Serializable
import javax.inject.Inject

/**
 * Created by nii on 12/3/17.
 */
class MathTaskFragment : Fragment() {
    lateinit var taskParams: TaskParams

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var viewModel: MathTaskViewModel

    val taskObserver = Observer<Task> { task -> updateView(task) }
    val timerObserver = Observer<Int> { time ->
        time?.let {
            if (time > 0) {
                view?.progressBar?.progress = time.toInt()
            } else {
                processAnswer()
            }
        }
    }

    companion object {
        fun newInstance(taskParams: TaskParams): MathTaskFragment {
            val fragment = MathTaskFragment()
            fragment.arguments = Bundle().apply {
                putSerializable("testData", taskParams as Serializable)
            }
            return fragment
        }

        const val TAG = "MathTaskSetupFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
        taskParams = arguments.getSerializable("testData") as TaskParams
        viewModel = ViewModelProviders.of(activity, viewModelFactory).get(MathTaskViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.controller_task_input, container, false)

        viewModel.currentTaskObservable.observe(this, taskObserver)
        viewModel.countdownProgress.observe(this, timerObserver)

        if (!viewModel.started) {
            viewModel.start(taskParams)
        }
        view.answer.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                processAnswer()
                return@OnKeyListener true
            }
            false
        })
        return view
    }

    private fun processAnswer() {
        view?.let {
            when (viewModel.checkAnswer(it.answer.text.toString())) {
                false -> notifyWrongAnswer()
            }
        }
    }

    private fun notifyWrongAnswer() {
        view?.let {
            it.error_bg.apply {
                alpha = 1f
                animate().alpha(0f).setDuration(500).start()
            }
        }

    }

    private fun updateView(task: Task?) {
        task?.let {
            view?.apply {
                task_text.text = it.task
                task_answer.text = it.answers
            }
        }
    }
}