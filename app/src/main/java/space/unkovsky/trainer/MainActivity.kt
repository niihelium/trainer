package space.unkovsky.trainer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import space.unkovsky.trainer.data.local.AppDatabase
import space.unkovsky.trainer.kotlin.replaceFragmentInActivity
import space.unkovsky.trainer.view.MainFragment

class MainActivity : AppCompatActivity() {

    lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        setupViewFragment()
    }

    private fun setupViewFragment() {
        supportFragmentManager.findFragmentById(R.id.contentFrame) ?:
                MainFragment.newInstance().let {
                    replaceFragmentInActivity(it, R.id.contentFrame)
                }
    }


//    override fun onNavigationItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            R.id.nav_stats -> {
//                router.pushController(RouterTransaction.with(SessionsController()))
//            }
//            R.id.nav_notes -> {
//                router.pushController(RouterTransaction.with(NotesController()))
//            }
//        }
//        //close navigation drawer
//        drawer_layout.closeDrawer(GravityCompat.START)
//        return true
//    }

    override fun onBackPressed() {

    }
}

