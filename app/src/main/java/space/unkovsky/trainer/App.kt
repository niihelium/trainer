package space.unkovsky.trainer

import android.app.Application
import space.unkovsky.trainer.di.AppComponent

/**
 * Created by nii on 12/3/17.
 */
class App : Application() {
    companion object {
        //platformStatic allow access it from java code
        @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = AppComponent.create(this, "database-name")
    }
}